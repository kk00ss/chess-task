Number of configuration for problem(7×7 board with 2 Kings, 2 Queens, 2 Bishops and 1 Knight) is 3063828, ~~solving took 25002ms (25sec), without multi-threading~~. 

~~**UPDATE 1: After replacing calls to println with StringBuilder and building the string outside of solve method time is down to about 10sec.**~~.

~~**UPDATE 2: Multi-threaded version of solver works only 1,5 times faster on my core i7 - about 6.45 seconds, probably because merging StringBuilder is slow, and shouldn't be performed at all**~~.

~~**UPDATE 3: Removed merging of StringBuilders - time required is down to 5.1 sec**.~~

**UPDATE 4: Fixed bug - Knight's location should be marked in BitSet (would output incorrect results with number of knights > 1) ~~makes for faster checking time was down to 4.5 seconds. ~~

Proper benchmarking - average time of 20 runs - is now 3.5 seconds.

UPDATE 5:
 ChessboardStates can be pre-allocated for each thread, only number of pieces +1 is actually needed, but it would require implementation of reseting it default state. This would reduce amount of work for GC, however bitsets are rather small, list for already placed pieces shares structure with previous state - performance improvement  could be neglectable.