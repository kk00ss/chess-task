import chessTask._
import ChessPieces.King
import Definitions._
import org.scalatest._
import scala.collection._

class KingTest extends FlatSpec {
    "King" should "be placed correctly" in {
        val a = new ChessBoardState(8,8)
        val test = a.trySetPiece(0,0, King)
        assert(test.nonEmpty)
    }
    it should "not be placed, when place is already occupied or threatened" in {
        var a = new ChessBoardState(8,8)
        a = a.trySetPiece(0,0, King).get
        val test1 = a.trySetPiece(0,0, King)
        assert(test1.isEmpty)
    }
    it should "not be placed, when it threatens already placed piece or position is threatened" in {
        var a = new ChessBoardState(8,8)
        a = a.trySetPiece(0,0, King).get
        val test2 = a.trySetPiece(1,1, King)
        assert(test2.isEmpty)
    }
    it should "be placed, when place is not threatened" in {
        var a = new ChessBoardState(8,8)
        a = a.trySetPiece(0,0, King).get
        val test2 = a.trySetPiece(2,2, King)
        assert(test2.isDefined)
    }
    it should "not be placed, when it threatens already placed piece" in {
        val board = new ChessBoardState(8,8, new mutable.BitSet(64) ,List(PlacedPiece(King,0,0)))
        assert(King.checkBoard(board, 1,1))
    }
    it should "be placed, when it doesn't threaten already placed piece" in {
        val board = new ChessBoardState(8,8, new mutable.BitSet(64) ,List(PlacedPiece(King,0,0)))
        assert(!King.checkBoard(board, 2,2))
    }

    it should "mark cells as threatened" in {
        val a = new ChessBoardState(4,4)
        val test = a.trySetPiece(3,3, King)
        test.get.printBoard
        val test1 = a.trySetPiece(0,0, King)
        test1.get.printBoard
        assert(test.get.getBoard == BitSet(10, 11, 14, 15))
        assert(test1.get.getBoard == BitSet(0, 1, 4, 5))
    }
}
