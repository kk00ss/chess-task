import chessTask.ChessPieces._
import chessTask.Definitions.SolverTask
import chessTask.Solver
import org.scalatest.FlatSpec

class SolverTest extends FlatSpec {
   "Solver" should "found exactly 92 configurations" in {
       val res = Solver.solve(SolverTask(8, 8,
           Array(Queen, Queen,Queen, Queen,Queen, Queen,Queen, Queen)))
       assert(res.numConfigurations == 92)
   }

}
