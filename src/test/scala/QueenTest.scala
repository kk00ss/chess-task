import chessTask.ChessBoardState
import org.scalatest._
import scala.collection._
import chessTask.ChessPieces._

class QueenTest  extends FlatSpec  {
    "Queen" should "be placed correctly" in {
        val a = new ChessBoardState(8,8)
        val test = a.trySetPiece(0,0, Queen)
        val expected = BitSet(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 16, 18, 24, 27, 32, 36, 40, 45, 48, 54, 56, 63)
        assert(test.get.getBoard == expected)
    }

    "Queen" should "not be placed, when position is threatened" in {
        val state = new ChessBoardState(8,8)
        val state2 = state.trySetPiece(4,4, Queen).get
        val test = state2.trySetPiece(4,5,Queen)
        assert(test.isEmpty)
    }
}
