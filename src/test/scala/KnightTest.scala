import chessTask.ChessBoardState
import chessTask.ChessPieces._
import org.scalatest.FlatSpec

import scala.collection._

class KnightTest extends FlatSpec  {
    "Knight" should "be placed correctly" in {
        val a = new ChessBoardState(8,8)
        val test = a.trySetPiece(4,4, Knight)
        assert(test.isDefined)
        assert(test.get.getBoard == BitSet(19, 21, 26, 30, 36, 42, 46, 51, 53))
    }
    "Knight" should "not be placed, when position is threatened" in {
        val a = new ChessBoardState(8,8)
        val state2 = a.trySetPiece(4,4, Knight).get
        val test2 = state2.trySetPiece(3,2, King)
        val test3 = state2.trySetPiece(2,3, King)
        assert(test2.isEmpty && test3.isEmpty)
    }
}
