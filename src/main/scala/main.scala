import chessTask._
import chessTask.Definitions._
import chessTask.ChessPieces._

object Main extends App {

    override def main(args: Array[String]): Unit = {
        val task = if(args.length > 0) {
            val x = args(0).toInt
            val y = args(1).toInt
            val pieces = args.drop(2).map(str => {
                val piece = str match {
                    case "King" => King
                    case "Queen" => Queen
                    case "Bishop" => Bishop
                    case "Rook" => Rook
                    case "Knight" => Knight
                    case x: String => throw new IllegalArgumentException(s"$x is not a chess piece " +
                        s"(pawn is not supported)")
                }
                piece
            })
            SolverTask(x,y,pieces)
        }
        else SolverTask(7, 7,
            Array(King, King, Queen, Queen, Bishop, Bishop, Knight))
        println(s"Solving task $task")
        SolverTest(task)
    }

    def SolverTest(task: SolverTask): Unit = {
        val start = System.currentTimeMillis()
        val result = Solver.solve(task)
        val time = System.currentTimeMillis() - start
        result.printConfigurations()
        println(s"number of configurations = ${result.numConfigurations}")
        println(s"solving took $time")
    }
}



