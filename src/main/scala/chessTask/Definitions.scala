package chessTask

import scala.collection._

object Definitions {

    case class SolverResult(numConfigurations: Int,
                            val configurations: List[String]) {
        def printConfigurations() = for (c <- configurations) println(c)
    }

    case class SolverTask(sizeX: Int,
                          sizeY: Int,
                          pieces: Seq[ChessPiece]) {
        val prioritizedPieces = pieces.sortWith(_.threatLevel > _.threatLevel)
    }

    trait chessBoard {
        val sizeX, sizeY: Int

        //tests if position described with i,j
        // is threatened by already placed pieces or occupied by one of them
        def checkForThreat(i: Int, j: Int): Boolean

        def checkExistingPieces(i: Int, j: Int): Boolean

        def setThreat(x: Int, y: Int)

        def trySetPiece(i: Int, j: Int, pieceType: ChessPiece): Option[chessBoard]
    }

    case class PlacedPiece(piece: ChessPiece, x: Int, y: Int) {
        override def toString = piece.toString + s"($x,$y)"
    }

    trait ChessPiece {
        val shortName: String
        val threatLevel: Int

        protected def mapThreats(board: chessBoard,
                                 x: Int, y: Int,
                                 //if action returns false - iteration will be stopped
                                 action: Function2[Int, Int, Boolean])

        //test if new piece threatens any of already existing pieces
        def checkBoard(board: chessBoard, x: Int, y: Int): Boolean = {
            var conflict = false
            mapThreats(board, x, y, (i, j) => {
                conflict = board.checkExistingPieces(i, j)
                conflict
            })
            conflict
        }

        def modifyBoard(board: chessBoard, x: Int, y: Int) = {
            mapThreats(board, x, y, (i, j) => {
                board.setThreat(i, j)
                val earlyStop = false
                earlyStop
            })
        }

        //common threats implementation
        protected def verticalHorizontalThreats(board: chessBoard,
                                                x: Int, y: Int,
                                                //if action returns false - iteration will be stopped
                                                action: Function2[Int, Int, Boolean]): Boolean = {

            var earlyStop = false
            //check horizontal
            var i = 0
            while (i < board.sizeX && !earlyStop) {
                earlyStop = action(i, y)
                i += 1
            }
            //check vertical
            if (!earlyStop) {
                var i = 0
                while (i < board.sizeY && !earlyStop) {
                    earlyStop = action(x, i)
                    i += 1
                }
            }
            earlyStop
        }

        protected def diagonalThreats(board: chessBoard,
                                      x: Int, y: Int,
                                      //if action returns false - iteration will be stopped
                                      action: Function2[Int, Int, Boolean]): Boolean = {
            var earlyStop = false
            //check growing diagonal
            if (!earlyStop) {
                var i = 0
                while (i < board.sizeY && !earlyStop) {
                    val j = x + y - i
                    if (j >= 0 && j < board.sizeY &&
                        action(i, j))
                        earlyStop = true
                    i += 1
                }
            }
            //check falling diagonal
            if (!earlyStop) {
                var i = 0
                while (i < board.sizeY && !earlyStop) {
                    val j = i - x + y
                    if (j >= 0 && j < board.sizeY &&
                        action(i, j))
                        earlyStop = true
                    i += 1
                }
            }
            earlyStop
        }
    }

}
