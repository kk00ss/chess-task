package chessTask

import chessTask.Definitions._

object ChessPieces {
    object King extends ChessPiece {
        val shortName = "ki"
        val threatLevel = 1
        override def toString = "King"
        protected def mapThreats(board: chessBoard,
                                 x: Int, y: Int,
                                 //if action returns false - iteration will be stopped
                                 action: Function2[Int,Int,Boolean]) = {
            var earlyStop = false
            var i= Math.max(x-1, 0)
            while(i < board.sizeX && i <= x+1 && !earlyStop) {
                var j = Math.max(y-1, 0)
                while(j < board.sizeX && j <= y+1 && !earlyStop) {
                    earlyStop = action(i,j)
                    j+=1
                }
                i+=1
            }
        }
    }

    object Queen extends ChessPiece {
        val shortName = "qe"
        val threatLevel = 4
        override def toString = "Queen"
        protected def mapThreats(board: chessBoard,
                                 x: Int, y: Int,
                                 //if action returns false - iteration will be stopped
                                 action: Function2[Int, Int, Boolean]) = {
            val earlyStop = verticalHorizontalThreats(board,x,y,action)
            if(!earlyStop)
                diagonalThreats(board,x,y,action)
        }
    }

    object Rook extends ChessPiece {
        val shortName = "ro"
        val threatLevel = 3
        override def toString = "Rook"
        protected def mapThreats(board: chessBoard,
                                 x: Int, y: Int,
                                 //if action returns false - iteration will be stopped
                                 action: Function2[Int, Int, Boolean])
        = verticalHorizontalThreats(board,x,y,action)
    }

    object Bishop extends ChessPiece {
        val shortName = "bi"
        val threatLevel = 3
        override def toString = "Bishop"
        protected def mapThreats(board: chessBoard,
                                 x: Int, y: Int,
                                 //if action returns false - iteration will be stopped
                                 action: Function2[Int, Int, Boolean])
        = diagonalThreats(board,x,y,action)
    }

    object Knight extends ChessPiece {
        val shortName = "kn"
        val threatLevel = 2
        override def toString = "Knight"
        private val threats = Array((1,2),(1,-2),(-1,2),(-1,-2),
                            (2,1),(-2,1),(2,-1),(-2,-1),
                            (0,0))
        protected def mapThreats(board: chessBoard,
                                 x: Int, y: Int,
                                 //if action returns false - iteration will be stopped
                                 action: Function2[Int, Int, Boolean]) = {
            var earlyStop = false
            var threatIndex = 0
            while (threatIndex < threats.length && !earlyStop) {
                val threat = threats(threatIndex)
                val x1 = x+threat._1
                val y1 = y+threat._2
                if(x1 >= 0 && y1 >= 0 && x1 < board.sizeX && y1 < board.sizeY)
                    earlyStop = action(x1,y1)
                threatIndex +=1
            }
        }
    }
}
