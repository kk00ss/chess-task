package chessTask

import chessTask.Definitions._

object Solver {
    case class SolverState
    (val boardHistory: List[ChessBoardState],
     val solutionsCount: Int = 0,
     val configurations: List[String] = List(),
     val currentPieceIndex: Int = 0,
     var completed: Boolean = false,
     val prevPosition: Int = -1)

    def solve(task: SolverTask): SolverResult = {
        var solverState = SolverState(List(new ChessBoardState(task.sizeX, task.sizeY)))
        //returned value indicates whether step was performed or not
        while (!solverState.completed)
            solverState = step(solverState, task)
        SolverResult(solverState.solutionsCount, solverState.configurations)
    }

    protected def step(prevState: SolverState, task: SolverTask): SolverState = {
        import task._
        //current state
        //meaningless comment
        var state = prevState
        val currentPiece = prioritizedPieces(state.currentPieceIndex)
        val newState = tryPlacePiece(state.boardHistory.head, currentPiece, state.prevPosition)
        //if we found solution
        if (state.currentPieceIndex == prioritizedPieces.length) {
            val discardedState = state.boardHistory.head
            val strBuilder = discardedState.getPlacedPieces.toString() :: state.configurations
            state = SolverState(
                state.boardHistory.tail,
                state.solutionsCount + 1,
                strBuilder,
                state.currentPieceIndex - 1,
                prevPosition = discardedState.lastPiecePosition)
        }
        if (newState.isDefined) {
            val newPieceIndex = state.currentPieceIndex + 1
            //if there is next piece
            val updatedPrevPosition = if (newPieceIndex < prioritizedPieces.length
                //and it is the same as the one we just placed
                && currentPiece == prioritizedPieces(newPieceIndex))
            // place next piece after previous
                newState.get.lastPiecePosition
            // start from the beginning of the board
            else -1
            state.copy(newState.get :: state.boardHistory,
                currentPieceIndex = newPieceIndex,
                prevPosition = updatedPrevPosition)
        }
        else {
            // step back
            val completed = state.currentPieceIndex == 0 && state.prevPosition == sizeX * sizeY - 1
            val newPrevPosition = if(!completed)
                state.boardHistory.head.lastPiecePosition
            else state.prevPosition
            state.copy(state.boardHistory.tail,
                currentPieceIndex =  state.currentPieceIndex - 1,
                prevPosition = newPrevPosition,
                completed = completed)
        }
    }

    protected def tryPlacePiece(baseState: ChessBoardState,
                      pieceType: ChessPiece,
                      previousPosition: Int): Option[ChessBoardState] = {
        var i = previousPosition + 1
        val lastPosition = baseState.sizeX * baseState.sizeY
        var newState: Option[ChessBoardState] = None
        while (i < lastPosition && newState.isEmpty) {
            val x = i / baseState.sizeX
            val y = i % baseState.sizeX
            newState = baseState.trySetPiece(x, y, pieceType)
            i += 1
        }
        newState
    }
}