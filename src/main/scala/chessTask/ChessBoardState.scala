package chessTask

import chessTask.Definitions._
import scala.collection._

case class ChessBoardState(sizeX: Int,
                           sizeY: Int,
                           private val board: mutable.BitSet,
                           private val placedPieces: List[PlacedPiece] = List()) extends chessBoard {
    def this(sizeX: Int,
             sizeY: Int) = this(sizeX, sizeY, new mutable.BitSet(sizeX * sizeY))

    def setThreat(x: Int, y: Int) = board += (x* sizeX + y)

    //tests if position described with i,j
    // is threatened by already placed pieces or occupied by one of them
    def checkForThreat(i: Int, j: Int): Boolean = board(i * sizeX + j)

    def checkExistingPieces(i: Int, j: Int) = placedPieces.exists(z => z.x == i && z.y == j)

    def trySetPiece(i: Int, j: Int, piece: ChessPiece): Option[ChessBoardState] = {
        if (i < 0 || j < 0 || i >= this.sizeX || j >= this.sizeY)
            throw new IllegalArgumentException("index out of bounds")
        if (!checkForThreat(i, j) && !piece.checkBoard(this, i, j)) {
            val copy = this.copy(placedPieces = PlacedPiece(piece, i, j) :: this.placedPieces,
                                 board = this.board.clone())
            piece.modifyBoard(copy,i,j)
            Some(copy)
        }
        else None
    }
    def lastPiecePosition: Int = placedPieces.head.x * sizeX + placedPieces.head.y

    //debug helpers
    def printPieces() = {
        println(this.placedPieces)
        println("========")
    }
    def getPlacedPieces = placedPieces
    def getBoard = this.board.clone()
    def printBoard() = println(board)
    def getExistingPiece(i: Int, j: Int) = placedPieces.find(z => z.x == i && z.y == j).get
    def drawBoard() = {
        for(i <- 0 until sizeX) {
            for (j <- 0 until sizeY) {
                val occupied = checkExistingPieces(i,j)
                if(occupied){
                    val pieceLabel = getExistingPiece(i,j).piece.toString
                    printf(pieceLabel+" ")
                }
                else if(board(i * sizeX+j)) printf("1  ") else printf("0  ")
            }
            println()
        }
        println("========")
    }
}